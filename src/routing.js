import VueRouter from 'vue-router'
import Users from './components/Users'
import Albums from './components/Albums'
import UserAdd from './components/UserAdd'
import User from './components/User'
import Vue from 'vue'
Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {path: '/users/add', component: UserAdd},
        {path: '/users/:id', component: User, name: 'show',
            children: [{
                path: 'edit', component: User, name: 'edit'
            }]
        },
        {path: '/users', component: Users},

        {path: '/albums', component: Albums},
    ],
    mode: 'history'
});