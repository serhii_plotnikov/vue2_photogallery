import {SEARCH_USER, ADD_USER, EDIT_USER, DELETE_USER} from './types/actions'

export default {

    [SEARCH_USER]: ({commit, getters}, {searchData, criterion}) => {
        return new Promise((resolve, reject) => {
            if (searchData === '') {
                reject();
                return;
            }
            let users = getters.getUsers;
            let regex = new RegExp("^" + searchData);
            let data = users.filter(user => {
                if (user[criterion].match(regex)) {
                    return user;
                }
            });
            if (data.length !== 0) {
                commit('SET_USER_SEARCH_CARD', data);
            } else {
                commit('RESET_USER_SEARCH_CARD');
            }
            resolve();
        });
    },
    [ADD_USER]: ({commit}, user) => {
        return new Promise(resolve => {
                commit("ADD_USER", user);
                resolve();
            }
        )
    },
    [EDIT_USER]: ({commit}, user) => {
        return new Promise(resolve => {
            commit('EDIT_USER', user);
            resolve();
        });
    },
    [DELETE_USER]: ({commit}, id) => {
        return new Promise(resolve => {
            commit('DELETE_USER', id);
            resolve();
        });
    }
};