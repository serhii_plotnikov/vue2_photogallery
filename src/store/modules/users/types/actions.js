export const ADD_USER = 'addUser';
export const SEARCH_USER = 'searchUser';
export const EDIT_USER = 'editUser';
export const DELETE_USER = 'deleteUser';