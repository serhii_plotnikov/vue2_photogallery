export const GET_USERS = 'getUsers';
export const GET_USER_BY_ID = 'getUserById';
export const GET_LAST_USER_ID = 'getLastUserId';