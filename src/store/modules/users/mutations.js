import Vue from "vue";
import {
    ADD_USER,
    DELETE_USER,
    EDIT_USER,
    SET_USER_SEARCH_CARD,
    RESET_USER_SEARCH_CARD
} from './types/mutations';

export default {
    [ADD_USER]: (state, user) => {
        Vue.set(state.users, state.users.length, user);
    },
    [DELETE_USER]: (state, id) => {
        let index = null;
        state.users.forEach((user, ind) => {
            if (user.id === id) {
                index = ind;
            }
        });
        state.users.splice(index, 1);
    },
    [EDIT_USER]: (state, data) => {
        let index = null;
        state.users.forEach((user, ind) => {
            if (user.id === data.id) {
                index = ind;
            }
        });
        Vue.set(state.users, index, data);
    },

    [SET_USER_SEARCH_CARD]: (state, data) => {
        state.searchUsersCards = [];
        data.map(user => {
            state.searchUsersCards.push({
                name: user.name,
                email: user.email,
                phone: user.phone
            });
        });

    },
    [RESET_USER_SEARCH_CARD]: (state) => {
        state.searchUsersCards = [];
    },
};
