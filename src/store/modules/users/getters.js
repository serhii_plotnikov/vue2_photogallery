import {GET_USER_BY_ID, GET_USERS, GET_LAST_USER_ID} from "./types/getters";

export default {
    [GET_USERS]: (state) => state.users,
    [GET_USER_BY_ID]: (state) => (userId) => {
        return state.users.find(user => user.id === userId);
    },
    [GET_LAST_USER_ID]:(state)=>{
        return state.users[state.users.length-1].id;
    }
}