import Vue from 'vue'
import state from './state'
import getters from './getters'


// const mutations = {
//     ADD_ALBUM(state, album) {
//         Vue.set(state.albums, state.albums.length, album);
//     },
//     DELETE_ALBUM(state, id) {
//         let index = null;
//         state.albums.forEach((album, ind) => {
//             if (album.id === id) {
//                 index = ind;
//             }
//         });
//         state.albums.splice(index, 1);
//     },
//     EDIT_ALBUM(state, data) {
//         let index = null;
//         state.albums.forEach((album, ind) => {
//             if (album.id === data.id) {
//                 index = ind;
//             }
//         });
//         Vue.set(state.albums, index, data);
//     },
//     GET_ALBUMS(state, data) {
//         data.forEach((item, index) => {
//             let album = {
//                 id: item.id,
//                 title: item.title,
//                 preview: item.preview,
//                 user_id: item.user_id
//             };
//             Vue.set(state.albums, index, album);
//         });
//     },
//     // SET_USER_SEARCH_CARD(state, data) {
//     //     state.searchUserCard = {
//     //         name: data.name,
//     //         email: data.email,
//     //         phone: data.phone
//     //     };
//     // },
//     // RESET_USER_SEARCH_CARD(state) {
//     //     state.searchUserCard = null;
//     // },
// };


export default {
    namespaced: true,
    state,
    getters,
    // mutations,
    // actions

}